// Practice 2 Task 2. Small game: "Guess the number!".
// You will play w/ AI. His name is "His Majesty the case".
// Diapason is from 0 to 99 for more represent outputs.
// Author: Oleg Ignatov <lego.ignatov@gmail.com>
// Created: 12.04.17

#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <time.h>

#define NN 100
#define N 10
#define OPENED_SYMBOL ' '
#define HIDDEN_SYMBOL '*'
#define CURRENT_SYMBOL 'X'
#define SLEEPING_TIME 800

/* �������� ����������� ���������� �������� ��� seed'� */
int gLastNumber;

/* �������� �������� �������� ���������� ������� */
int minPtrH, maxPtrH, minPtrC, maxPtrC;

char masH[N][N];
char masC[N][N];
int round = 1;
int scoresH = 0;
int scoresC = 0;
int scoresBank = 10;

/* �������� ������� ��� ��������� ��������� ����� */
int getRand (int numMinimum, int numLimit);
/* �������� ������� ���������� ������� ���������� */
void updateMas (char * mas, int curNumber, int hideNumber, int minPtr, int maxPtr, unsigned char is_human);
/* �������� ������� ��������� ������ */
void draw_tabels ();
/* �������� ������� ��������� ������� ������ */
void draw_game();
/* �������� ������� ��� ��������� ������ ������� */
void updateSymbol (char * mas, int updatedNum, char newSymbol);
/* �������� ������� ��� �������� ��������� �������� ����� */
int checkRanges(int checkNum, int minRange, int maxRange);

int main()
{
	int i, j, numH, numC, curNumH, curNumC, tmpVar, curTurn, tmpBankUpdate;
	
	/* ������������� ��� ��������� ��������� ����� */
	srand(time(NULL));
	gLastNumber = rand();
	/* ������ ����� ������������ getRand(<MIN_limit>, <MAX_limit>) */	

	while ( round != 4 ) {

		minPtrH = minPtrC = 0;
		maxPtrH = maxPtrC = NN-1;

		for (i=0; i<N; i++)
			for (j=0; j<N; j++) {
				masH[i][j] = masC[i][j] = HIDDEN_SYMBOL;
		}

		numH = getRand(0, 99);
		numC = getRand(0, 99);

		updateSymbol(&masC[0][0], numC, 'N');

		curTurn = 0;
		curNumH = curNumC = -1;
		tmpBankUpdate = 0;

		while ( curNumH != numH && curNumC != numC ) {
			if ( curTurn == 0 ) {
				tmpVar = 0;
				do {
					system("cls");
					draw_game();
					if ( tmpVar == 0 ) {
						printf("Guess the number: ");
						tmpVar = 1;
					} else { printf("Enter corrent number in range [%d..%d]: ", minPtrH, maxPtrH); }
					scanf("%d",&curNumH);
				} while ( checkRanges(curNumH, minPtrH, maxPtrH) );
			} else {
				curNumC = getRand(minPtrC, maxPtrC);
			}

			printf("\a");

			if ( curTurn == 0 ) {updateSymbol(&masH[0][0], curNumH, 'X'); }
			else { updateSymbol(&masC[0][0], curNumC, 'X'); }

			system("cls");
			draw_game();
			Sleep(SLEEPING_TIME);
	
			if ( curNumH == numH || curNumC == numC ) {
				printf("\a\a\a");
				if ( curTurn == 0 ) { printf("Human won!\n"); }
				else { printf("Computer won this round!\n"); }
				Sleep(SLEEPING_TIME);
			} else {
				if ( curTurn == 0 ) { updateMas(&masH[0][0],curNumH,numH,minPtrH,maxPtrH,1); }
				else { updateMas(&masC[0][0],curNumC,numC,minPtrC,maxPtrC,0); }
			}	
			system("cls");
			draw_game();
			Sleep(SLEEPING_TIME);

			if ( curTurn == 0 ) { curTurn = 1; }
			else if ( curTurn == 1 ) { curTurn = 0; }
			tmpBankUpdate++;
			if ( tmpBankUpdate == 2 ) {
				scoresBank += 10;
				tmpBankUpdate = 0;
			}
		}
		//-------
		if ( curNumH == numH ) scoresH += scoresBank;
		if ( curNumC == numC ) scoresC += scoresBank;

		scoresBank = 10;
		round++;
	}
	
	system("cls");
	draw_game();

	if ( scoresH > scoresC ) { printf("  You won! My congratulations! :)\n\n"); }
	else if ( scoresH == scoresC ) { printf("  Draw! Very hard game! :)\n\n"); }
	else { printf("  Computer won! :) Try again, maybe in the nex time you will win... \n\n"); }

	Sleep(SLEEPING_TIME*2);

	return 0;
}

/* ���������� ������� ��������� ���������� ����� */
int getRand (int numMinimum, int numLimit) {
	srand(gLastNumber);
	gLastNumber = rand();
	return (gLastNumber % (numLimit-numMinimum+1)) + numMinimum;
}

/* ���������� ������� ���������� ������� ���������� */
void updateMas (char * mas, int curNumber, int hideNumber, int minPtr, int maxPtr, unsigned char is_human) {
	int idxI, idxJ;
	
	if ( curNumber < hideNumber ) {
		while ( minPtr < curNumber+1 ) {
			idxI = minPtr / N;
			idxJ = minPtr % N;
			*(mas + idxI*N + idxJ) = OPENED_SYMBOL;
			minPtr++;
		}
		if ( is_human == 1 ) { minPtrH = minPtr; }
		else { minPtrC = minPtr; }
	} else {
		while ( maxPtr > curNumber-1 ) {
			idxI = maxPtr / N;
			idxJ = maxPtr % N;
			*(mas + idxI*N + idxJ) = OPENED_SYMBOL;
			maxPtr--;
		}
		if ( is_human == 1 ) { maxPtrH = maxPtr; }
		else { maxPtrC = maxPtr; }
	}	
}

/* ���������� ������� ��������� ������ */
void draw_tabels () {
	int k=0, t=0, m=0;
	for(k=0; k<10; k++) printf(" ");
	printf("0 1 2 3 4 5 6 7 8 9");
	for (k=0; k<20; k++) printf(" ");
	printf("0 1 2 3 4 5 6 7 8 9\n");

	for (t=0; t<N; t++) {
		for(k=0; k<7; k++) printf(" ");
		printf("%2d ", t*10);
		//1
		for (m=0; m<N; m++) {
			printf("%c ", *(&masH[0][0] + t*N + m));
		}		
		if ( t == 2 ) {
			if ( round == 4 ) {
				printf("    Round X     ");
			} else { printf("    Round %d     ", round); }
		}
		else  if ( t == 4 ) {
			if ( round == 4 ) {
				printf("    Bank XX     ");
			} else { printf("    Bank %2d     ", scoresBank); }
		}
		else for (k=0; k<16; k++) printf(" ");

		printf("%2d ", t*10);
		//2
		for (m=0; m<N; m++) {
			printf("%c ", *(&masC[0][0] + t*N + m));
		}
		printf("\n");
	}
	printf("\n");
}

/* ���������� ������� ��������� ������� ������ */
void draw_game() {
	int k=0;
	printf("Your score: %3d", scoresH);
	for(k=0; k<45; k++) printf(" ");
	printf("%3d  :Computer score", scoresC);
	for(k=0; k<27; k++) printf(" ");
	printf("Game \"Guess the number!\"\n\n");
	draw_tabels();
	printf("   Rules for this game is very simple. You play against a computer. " \
		   "His other\n  name is \"His Majesty the Case\". Try to guess the number first and bit " \
		   "the\n  Computer during 3 rounds.  Hidden numbers are different.  You can see his\n  number, " \
		   "but he cannot. Check your intuition up! It's a fair game.\n\n");
}

/* �������� ������� ��� ��������� ������ ������� */
void updateSymbol (char * mas, int updatedNum, char newSymbol) {
	int idxI, idxJ;
	idxI = updatedNum / N;
	idxJ = updatedNum % N;
	*(mas + idxI*N + idxJ) = newSymbol;
}

/* �������� ������� ��� �������� ��������� �������� ����� */
int checkRanges(int checkNum, int minRange, int maxRange) {
	if ( checkNum < minRange || checkNum > maxRange ) { return 1; }
	else { return 0; }
}