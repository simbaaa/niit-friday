// Practice 3 Task 2. A work w/ strings.
// Author: Oleg Ignatov <lego.ignatov@gmail.com>
// Created: 23.04.17
#include <stdio.h>

#define N 128

int main()
{
	char str[N-1], word[N-1];
	int inWord, i, count, length, lenIdx;

	printf("Enter string:");
	fgets(str,N,stdin);

	printf("Word statistic:\n # of symbols   Word\n");

	inWord = 0;
	i = 0;
	count = 0;	
	
	while  (str[i]!='\0')
	{
		if (str[i]!=' ' && str[i]!='\t' && str[i]!='\n') {
			if ( inWord == 0 ) {
				count++;
				inWord=1;
				length = 0;
				lenIdx = 0;
			}
			length++;
			word[lenIdx] = str[i];
		} else if (str[i]==' ' || str[i]=='\t' || str[i]=='\n') { 
			if ( inWord == 1 ) {
				inWord=0;
				word[lenIdx] = '\0';
				printf(" %12d   %s\n", length, word);
			}			
		}
		i++;
		lenIdx++;
	}

	printf("Number of words: %d\n",count);

	return 0;
}