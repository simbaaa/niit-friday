// Practice 2 Task 1. Flying bomb in real time.
// Author: Oleg Ignatov <lego.ignatov@gmail.com>
// Created: 09.04.17

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <Windows.h>

#define G 9.80665
#define HALF_G 4.903325
#define UPDATE_TIME_ENGIN 200

#define SMALL_CINEMA

#ifdef SMALL_CINEMA
#define OUTPUT_SLEEP_TIME 1000
void draw_scheme (double current_height, double start_height);
void draw_ending ();
void draw_ground ();
#endif

int main ()
{
	double sHeight, cHeight, cVelo, eTimeSec;
	int sTimeClocks, eTimeClocks;

	printf("Enter height: ");
	scanf("%lf",&sHeight);

	cHeight = sHeight;

	sTimeClocks = clock();

	while ( cHeight > 0 ) {
		eTimeClocks = clock() - sTimeClocks;
		eTimeSec = (double)eTimeClocks / CLOCKS_PER_SEC;
		cVelo = (double)(G * eTimeSec);

		cHeight = sHeight - HALF_G * powf(eTimeSec,2.0);
		if ( cHeight > 0 ) {
			system("cls");
			printf("Time: %.2lf    Speed: %8.2lf m/sec    Current height: %.2lf m\n", eTimeSec, cVelo, cHeight);
#ifdef SMALL_CINEMA
			draw_scheme(cHeight,sHeight);
#endif
		} else {
#ifdef SMALL_CINEMA
			draw_ending();
#endif
			printf("\nStart height: %.2lf m\n",sHeight);
			printf("Elepsed time: %.2lf sec\n",sqrt(2*sHeight/G));
			printf("Maximal bomb's speed: %.2lf m/sec\n\n",sqrt(2*sHeight/G)*G);
		}
		Sleep(UPDATE_TIME_ENGIN);
	};

	return 0;
}
#ifdef SMALL_CINEMA
void draw_scheme (double cHeight_local, double sHeight_local) {
	int p, i;
	double p_double = cHeight_local / sHeight_local * 10;

	p = 10 - (int)p_double;
	for (i=0; i<p; i++) { printf("\n"); }
	
	printf("%38s\\/\n%38s/\\\n%37s(  )\n%38s\\/\n"," "," "," "," ");

	for (i=10; i>p; i--) { printf("\n"); }
	draw_ground();
}

void draw_ending() {
	int i;

	system("cls");
	printf("\n");
	draw_scheme(0.00001f,10000.0f);
	Sleep(OUTPUT_SLEEP_TIME*2);

	system("cls");
	for (i=0; i<150; i++) { printf("********"); }
	draw_ground();
	Sleep(OUTPUT_SLEEP_TIME*2);

	system("cls");
	for (i=0; i<240; i++) { printf("\xB0\xB0\xB0\xB0\xB0"); }
	draw_ground();
	Sleep(OUTPUT_SLEEP_TIME*2);

	system("cls");
	for (i=0; i<119; i++) { printf("     "); }
	printf("Stupid war");
	for (i=0; i<119; i++) { printf("     "); }
	draw_ground();
	Sleep(OUTPUT_SLEEP_TIME);
}

void draw_ground () {
	int i;
	for (i=0; i<10; i++) { printf("========"); }
	for (i=0; i<10; i++) { printf("////////"); }
}
#endif
//EOF