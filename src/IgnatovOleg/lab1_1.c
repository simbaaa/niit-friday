//������������ ������ �1
//
//TASK: 1
//DESCRIPTION: �������� ���������, ������� ����������� � ������������ ���,
//���� � ���, � ����� ����������� ����������� ����� � ����, ������� ������������ � ����������
//��������� (��������, ����������, �����)
//OWNER: ���� �������
//

#include <stdio.h>

int main ()
{
	char gender;
	float growth = 0.0f;
	float weight = 0.0f;
	float coef = 0.0f;
	float balance = 0.0f;

	printf("Enter your gender [m/f]: ");
	scanf("%s",&gender);
	printf("Ender your growth: ");
	scanf("%f",&growth);
	printf("Enter your weight: ");
	scanf("%f",&weight);

	if (gender == 'm') {
		coef = 100.0f;
	} else { coef = 110.0f; }

	balance = growth - coef;
	
	if (balance > weight) {
		printf("Please gain weight on %.2f kg\n",balance-weight);
	} else if (balance < weight) {
		printf("Please lose weight on %.2f kg\n",weight-balance);
	} else printf("You have perfect weight. Continue in the same spirit!\n\n");

	return 0;
}