#include <stdio.h>

#define N 128

int main()
{
	char str[N-1];
	int inWord, i, count;

	printf("Enter string:");
	fgets(str,N,stdin);


	inWord = 0;
	i = 0;
	count = 0;
	while  (str[i]!='\0')
	{
		if ((str[i]!=' ' && str[i]!='\t' && str[i]!='\n') && inWord==0) {
			count++;
			inWord=1;
		} else if ((str[i]==' ' || str[i]=='\t' || str[i]=='\n') && inWord==1) {
			inWord=0;
		}
		i++;
	}

	//printf("%s",str);
	printf("Number of words is %d\n",count);

	return 0;
}