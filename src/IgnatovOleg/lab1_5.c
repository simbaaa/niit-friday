////////////////////////////////////////////////////////////////////////////////
//                              Practice No.2                                 //
//                                                                            //
//        Task: 5                                                             //
// Description: Write a program that takes a string from the user and dis-    ////              plays it on the screen aligning it to the center.             //
//      Author: Oleg Ignatov (lego.ignatov@gmail.com)                         //
//        Data: 08.04.2017                                                    //
////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>

int main()
{
	char str[81];

	printf("> Enter string (max lenght is 80 characters):\n");
	fgets(str,81,stdin);	
	printf("\n%*s\n",40+(int)((strlen(str)-1)/2),str);

	return 0;
}
