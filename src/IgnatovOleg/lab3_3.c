// Practice 3 Task 3. A work w/ strings.
// Author: Oleg Ignatov <lego.ignatov@gmail.com>
// Created: 23.04.17
#include <stdio.h>

#define N 128

int main()
{
	char str[N-1], word[N-1], maxWord[N-1];
	int inWord, i, count, length, lenIdx, maxLen, j;

	printf("Enter string:");
	fgets(str,N,stdin);

	printf("Word w/ max length (the last if there are a few words):\n # of symbols   Word\n");

	inWord = 0;
	i = 0;
	count = 0;
	maxLen = 0;
	
	while  (str[i]!='\0')
	{
		if (str[i]!=' ' && str[i]!='\t' && str[i]!='\n') {
			if ( inWord == 0 ) {
				count++;
				inWord=1;
				length = 0;
				lenIdx = 0;
			}
			length++;
			word[lenIdx] = str[i];
		} else if (str[i]==' ' || str[i]=='\t' || str[i]=='\n') { 
			if ( inWord == 1 ) {
				inWord=0;
				word[lenIdx] = '\0';
				if ( lenIdx >= maxLen ) {
					j = 0;
					while ( word[j] != '\0' ) {
						maxWord[j] = word[j];
						j++;
					}
					maxWord[j] = '\0';
					maxLen = lenIdx;					
				}
			}			
		}
		i++;
		lenIdx++;
	}
	printf(" %12d   %s\n", maxLen, maxWord);
	return 0;
}
